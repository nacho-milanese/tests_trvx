# Exercise:

This is just some useless exercise.

For it to work, you need Vagrant + VirtualBox and Ansible installed.

Vagrant will setup 3 servers (1 in fact as other two are commented)
[in order to create 3 servers, uncomment lines 7 and 8 in Vagrantfile]

IF 3 SERVERS WERE TO BE CREATED, comment line 57 and 58 in playbook_master

Check on networking configuration in Vagrant file to see if it adjusts to your requirements.


To run, excecute:

`vagrant up`

On any issue, you can repeat the provisioning step with:

`vagrant provision {server_name}`

To connect a server, perform:

`vagrant ssh {server_name}`
